<?php 

namespace Grit;

class Form
{
    protected $fields;
    
    public function __construct(array $fields)
    {
       // dump($fields);

        $this->fields = $fields;
    }

    public static function make( array $fields)
    {
        return new Form($fields);
    }

    public function display()
    {
        $output = "<form>";

        foreach ($this->fields as $field)
        {
            $output .= $field->display();
        }

        $output .= "</form>";

        return $output;
    }
}