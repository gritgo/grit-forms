<?php

namespace Grit\Fields;

class Checkbox extends Field
{
    protected $type = "checkbox";

    public function set_option_value( $value )
    {
        $this->option_value = $value;

        return $this;
    }
}