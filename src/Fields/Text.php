<?php

namespace Grit\Fields;

class Text extends Field
{
    protected $type = "text";
}