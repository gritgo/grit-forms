<?php 

namespace Grit\Fields;

use Grit\Fields\Text;

class Field
{
    protected $type;
    protected $name;
    protected $label = false;
    protected $option_value = false;
    
    // protected $options;

    public function __construct( $name, $label = false)
    {
        $this->name = $name;
        $this->label = $label;

        return $this;
    }

    public static function make( $type, $name, $label = false )
    {
        $type = ucfirst($type);

        $class = 'Grit\\Fields\\'.$type;

        return new $class($name, $label);
    }

    public function display()
    {

        $out = "";
        if( $this->label) {
            $out .= '<label for="' . $this->name . '">' . $this->label . '</label>';
        }

        $out .= '<input type="'. $this->type . '" name="' . $this->name . '"';

        if( $this->option_value ) {
            $out .= ' value="' . $this->option_value . '"';
        }

        $out .= '>';
        
        
        return $out; 
        
    }
}