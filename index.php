<?php

use Grit\Form;
use Grit\Fields\Field;

include_once "vendor/autoload.php";

$form = Form::make([
    Field::make('text', "bumbar", "Bumbar"),
    Field::make('checkbox',"wazaa", "Wazaa")->set_option_value("yes")
]);

echo $form->display();